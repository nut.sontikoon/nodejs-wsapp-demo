/*

WebApp for Kubernetes demo
Nuttapong Sontikoon
15 OCT 2018


. */

var express = require('express');
var router = express.Router();


/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'Express' });
});


module.exports = router;


