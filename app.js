var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var redis = require("redis");
var redisStore = require('connect-redis')(session);
var mysql = require('mysql');
var os = require('os');


//var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
//var homeRoute = require('./routes/home');

var async = require("async");

var app = express();


/// Env variables //
const REDIS_HOST = process.env.REDIS_HOST || '172.17.17.11';
const REDIS_PORT = process.env.REDIS_PORT || 6379;
const REDIS_TIMEOUT = process.env.REDIS_TIMEOUT || 600; // 10 Min
const MYSQL_HOST = process.env.MYSQL_HOST || '172.17.17.12';
const MYSQL_PORT = process.env.MYSQL_PORT || 3306;
const MYSQL_USER = process.env.MYSQL_USER || 'root';
const MYSQL_PASS = process.env.MYSQL_PASS || 'Datacraft!';
const MYSQL_DB = process.env.MYSQL_DB || 'wsdb';


var client = redis.createClient(REDIS_PORT,REDIS_HOST);
client.auth('ddth1234',function(err,reply) {
    console.log("Redis authen log : "+reply);
});


// Always use MySQL pooling.
// Helpful for multiple connections.

var dbpool	=	mysql.createPool({
    connectionLimit : 100,
    host     : MYSQL_HOST,
    user     : MYSQL_USER,
    password : MYSQL_PASS,
    database : MYSQL_DB,
    debug    :  false
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret: 'websecerts',
    store: new redisStore({ host: REDIS_HOST, port: REDIS_PORT, client: client,ttl :  6000}),
    saveUninitialized: false,
    resave: false
}));


app.use(cookieParser("secretSign#143_!223"));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// function for redis //
client.on('connect',function () {
    console.log("Init Redis Connect : Redis connected Redis Host : "+REDIS_HOST);
});

client.on('error',function (err) {
    console.log("Init Redis Connect : Connection error Redis Host : " + REDIS_HOST +" Error Message : " +err);
    throw err;
});


// database handle
function handle_database(req,type,callback) {
    async.waterfall([
        function(callback) {
            dbpool.getConnection(function(err,connection){
                if(err) {
                    // if there is error, stop right away.
                    // This will stop the async code execution and goes to last function.
                    console.log('Get connection to DB Error : DB Host : '+MYSQL_HOSTS+ " " )
                    callback(true);
                } else {
                    console.log('Get connection to DB Host : '+ MYSQL_HOST+" Success!!!")
                    callback(null,connection);
                }
            });
        },
        function(connection,callback) {
            var SQLquery;
            switch(type) {
                case "login" :
                    SQLquery = "SELECT * from user_login WHERE user_email='"+req.body.user_email+"' AND `user_password`='"+req.body.user_password+"'";
                    break;
                case "checkEmail" :
                    SQLquery = "SELECT * from user_login WHERE user_email='"+req.body.user_email+"'";
                    break;
                case "register" :
                    SQLquery = "INSERT into user_login(user_email,user_password,user_name) VALUES ('"+req.body.user_email+"','"+req.body.user_password+"','"+req.body.user_name+"')";
                    break;
                case "addStatus" :
                    SQLquery = "INSERT into user_status(user_id,user_status) VALUES ("+req.session.key["user_id"]+",'"+req.body.status+"')";
                    break;
                case "getStatus" :
                    SQLquery = "SELECT * FROM user_status WHERE user_id="+req.session.key["user_id"];
                    break;
                default :
                    break;
            }
            callback(null,connection,SQLquery);
        },
        function(connection,SQLquery,callback) {
            connection.query(SQLquery,function(err,rows){
                connection.release();
                if(!err) {
                    if(type === "login") {
                        callback(rows.length === 0 ? false : rows[0]);
                    } else if(type === "getStatus") {
                        callback(rows.length === 0 ? false : rows);
                    } else if(type === "checkEmail") {
                        callback(rows.length === 0 ? false : true);
                    }else {
                        callback(false);
                    }
                } else {
                    // if there is error, stop right away.
                    // This will stop the async code execution and goes to last function.
                    callback(true);
                }
            });
        }
    ],function(result){
        // This function gets call after every async task finished.
        if(typeof(result) === "boolean" && result === true) {
            callback(null);
        } else {
            callback(result);
        }
    });
}


//////// Route //////////////

//app.use('/', indexRouter);
app.use('/users', usersRouter);
//app.use('/home', homeRoute);

app.post('/login',function(req,res){
    handle_database(req,"login",function (result) {
        console.log("Log with query database result : "+result);
        if(result==null){
            res.json({"error" : "true","message" : "Database error occured"});
            console.log("Login Function : Error can not connect to DB");
        }
        else{
            if(!result){
                res.json({"error":true,"message":"Login failed. Please register user first."});
                console.log("Login function : Error ")
            }
            else{
                req.session.key = result;
                res.json({"error":false,"message:" :"Loging Sucess."});
                console.log("Login function : Login sucess."+JSON.stringify(result));
            }
        }
    })

    //req.session.key = "aaaa";
    //res.json({"error" : false,"message" : "Login success."});
});

app.get('/home', function(req, res, next) {
    if(req.session.key) {
        res.render('home', { "user_email" : req.session.key["user_email"],servername: os.hostname()});
    } else {
        res.redirect("/");
    }
});

/* GET home page. */
app.get('/', function(req, res, next) {
    if(req.session.key){
        res.redirect('/home');
    }
    else {
        res.render('index', { title: 'Web front End Demo' ,servername: os.hostname()});
    }

});

app.get('/logout',function (req, res, next) {
    if(req.session.key){
        req.session.destroy(function () {
            res.redirect('/');
        });
    }
    else {
        res.redirect('/');
    }

});

app.get('/fetchStatus',function (req, res, next) {
    if(req.session.key) {
        handle_database(req,"getStatus",function(response){
            if(!response) {
                res.json({"error" : false, "message" : "There is no status to show."});
                console.log("Fetch Status Function :  There is no status to show")
            } else {
                res.json({"error" : false, "message" : response});
                console.log("Fetch Status Function : Response message User : " +req.session.key["user_email"]+ JSON.stringify(response));
            }
        });
    } else {
        res.json({"error" : true, "message" : "Please login first."});
        console.log("Fetch Status Function : Error Please login first.")
    }
});

app.post("/addStatus",function(req,res){
    if(req.session.key) {
        handle_database(req,"addStatus",function(response){
            if(!response) {
                res.json({"error" : false, "message" : "Status is added."});
            } else {
                res.json({"error" : false, "message" : "Error while adding Status"});
            }
        });
    } else {
        res.json({"error" : true, "message" : "Please login first."});
    }
});

app.post("/register",function(req,res){
    handle_database(req,"checkEmail",function(response){
        if(response === null) {
            res.json({"error" : true, "message" : "This email is already present"});
        } else {
            handle_database(req,"register",function(response){
                if(response === null) {
                    res.json({"error" : true , "message" : "Error while adding user."});
                } else {
                    res.json({"error" : false, "message" : "Registered successfully."});
                }
            });
        }
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
